package com.qa.util;

public class TestUtil {

    public static long PAGE_LOAD_TIMEOUT = 20;
    public static long IMPLICIT_WAIT = 10;

    public static final String COMPANY = "https://www.musala.com/company/";
    public static final String FACEBOOK = "https://www.facebook.com/MusalaSoft?fref=ts";
    public static final String FACEBOOK_PROFILE_PICTURE = "https://scontent.fskp1-1.fna.fbcdn.net/v/t1.0-1/p200x200/158325737_3926723744014946_1132226306152824042_o.jpg?_nc_cat=100&ccb=1-3&_nc_sid=dbb9e7&_nc_ohc=YpQiohUbR6sAX8_dTQF&_nc_ht=scontent.fskp1-1.fna&tp=6&oh=927f1f5bf2b83b14a4eacb81c4ce85c6&oe=6084A8A8";
    public static final String CAREERS_PAGE = "https://www.musala.com/careers/join-us/";

    public static final String REQUIRED_EMAIL_WARNING = "The e-mail address entered is invalid.";
    public static final String REQUIRED_FIELD_WARNING = "The field is required.";
}
