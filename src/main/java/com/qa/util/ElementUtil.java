package com.qa.util;

import com.qa.base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ElementUtil extends TestBase {

    public void clickElement(WebElement element) {
        waitForElementToBecomeClickable(driver, TestUtil.IMPLICIT_WAIT, element);
        element.click();
    }

    public void scrollAndClickElement(WebElement element) {
        waitForElementToBecomeClickable(driver, TestUtil.IMPLICIT_WAIT, element);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
        element.click();
    }

    public void setText(WebElement element, String text) {
        waitForElementToBeVisible(driver, TestUtil.IMPLICIT_WAIT, element);
        element.sendKeys(text);
    }

    public String getElementText(WebElement element) {
        waitForElementToBeVisible(driver, TestUtil.IMPLICIT_WAIT, element);
        System.out.println(element.getText());
        return element.getText();
    }

    public void switchBrowserTabs(int tab) {
        ArrayList<String> nextTab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(nextTab.get(tab));
    }

    public void CheckImage(WebElement element) {
        Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver)
                .executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", element);
        if (!ImagePresent) {
            System.out.println("Image not displayed.");
        } else {
            System.out.println("Image displayed.");
        }
    }

    public void listElements(List<WebElement> elements, String text) {
        String myLocation = "";
        for (WebElement elem : elements) {
                myLocation = elem.getText();
                if (myLocation.equals(text)) {
                    elem.click();
                    break; // DO NOT REMOVE THIS IT WILL THROW STALE ELEMENT EXCEPTION
            }
        }
        Assert.assertEquals(myLocation, text);
    }

}
