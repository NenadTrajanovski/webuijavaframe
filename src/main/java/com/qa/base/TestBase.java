package com.qa.base;

import com.qa.util.TestUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static WebDriver driver;
    public static Properties prop;

    public TestBase(){

        try {
            prop = new Properties();
            FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\java\\com\\qa\\config\\config.properties");
            prop.load(fis);
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public static void initialization(){
        /**
         * On purpose drivers are set different way to show both examples of:
         *      Having driver set under environment variables
         *      Having driver stored as file within the frame
         */
        String browserName = prop.getProperty("chrome");

        if (browserName.equals("chrome")){
            //USE setProperty if web driver is not set under environment variables
            //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\webdrivers\\chromedriver.exe");
            driver = new ChromeDriver();
        }else if (browserName.equals("firefox")){
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\webdrivers\\geckodriver.exe");
            driver = new FirefoxDriver();
        }

        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
        driver.get(prop.getProperty("url"));
    }

    //DATA PROVIDER

    @DataProvider
    public Object[] readUserLoginData() throws IOException, ParseException {
        JSONParser jp = new JSONParser();
        FileReader reader = new FileReader("src\\main\\java\\com\\qa\\util\\invalidEmails.json");

        Object obj = jp.parse(reader);

        JSONObject emailJsonObj = (JSONObject) obj;
        JSONArray emailLoginArr = (JSONArray) emailJsonObj.get("invalidEmails");

        String arr[] = new String[emailLoginArr.size()];

        for (int i = 0; i < emailLoginArr.size(); i++){
            JSONObject emails = (JSONObject) emailLoginArr.get(i);
            String user = (String) emails.get("email");
            arr[i] = user;
        }
        return arr;
    }

    //WAIT TYPES

    public void waitPageToLoad(WebDriver driver) {
        // need to re-read JS things
        String pageLoadStatus;
        do {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            pageLoadStatus = (String) js.executeScript("return document.readyState");
        } while (!pageLoadStatus.equals("complete"));
    }


    public void waitForElementToBecomeClickable(WebDriver driver, long timeout, WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }


    public WebElement waitForElementToBeVisible(WebDriver driver, long timeout, WebElement element){
        WebElement elementOne = null;
        try {
            System.out.println("Waiting for max:: " + timeout + " seconds");
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            elementOne = wait.until(ExpectedConditions.visibilityOf(element));
            System.out.println("Element " + element.getText() + " appeared on the page");
        } catch (Exception e){
            System.out.println("Element " + element.getText()+ " didn't appear on the page");
        } return elementOne;
    }

    public void waitForTextToBePresentInElement(WebDriver driver, long timeout, WebElement element, String text) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.textToBePresentInElement(element, text));
    }
}
