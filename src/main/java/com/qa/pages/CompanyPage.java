package com.qa.pages;

import com.qa.base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CompanyPage extends TestBase {

    //Elements

    @FindBy(xpath = "//h2[text() = 'Leadership']")
    public WebElement leadershipHeader;

    @FindBy(css = ".musala-icon-facebook")
    public WebElement facebook;

    @FindBy(xpath = "//a[@aria-label = 'Profile picture']//img")
    public WebElement facebookProfilePicture;

    //Init Elements
    public CompanyPage(){
        PageFactory.initElements(driver, this);
    }
}

