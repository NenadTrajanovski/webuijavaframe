package com.qa.pages;

import com.qa.base.TestBase;
import com.qa.util.TestUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.ElementUtil;
import org.testng.Assert;
import sun.awt.windows.WBufferStrategy;

import java.io.File;
import java.util.List;

public class CareersPage extends TestBase {

    ElementUtil el = new ElementUtil();

    //ELEMENTS

    @FindBy(xpath = "//span[text() = 'Check our open positions']")
    public WebElement openPositions;

    @FindBy(xpath = "//select[@id = 'get_location']/option")
    public List<WebElement> locations;

    @FindBy(xpath = "//img[contains(@alt, 'Experienced QA Engineer')]")
    public WebElement qaEngineer;


    @FindBy(xpath = "//h2[text() = 'General description']")
    public WebElement generalDescription;

    @FindBy(xpath = "//h2[text() = 'Requirements']")
    public WebElement requirements;

    @FindBy(xpath = "//h2[text() = 'Responsibilities']")
    public WebElement responsibilities;

    @FindBy(xpath = "//h2[text() = 'What we offer']")
    public WebElement whatWeOffer;

    @FindBy(xpath = "//input[@value = 'Apply']")
    public WebElement applyBtn;

    @FindBy(xpath = "//input[@name = 'your-name']")
    public WebElement careersName;

    @FindBy(xpath = "//input[@name = 'your-email']")
    public WebElement careersEmail;

    @FindBy(xpath = "//input[@name = 'mobile-number']")
    public WebElement careersMobile;

    @FindBy(id = "uploadtextfield")
    public WebElement careersUploadCv;

    @FindBy(xpath = "//textarea[@name = 'your-message']")
    public WebElement careersYourMessage;

    @FindBy(xpath = "//input[@type= 'submit']")
    public WebElement careersSendBtn;

    @FindBy(xpath = "//button[@class = 'close-form']")
    public WebElement careersCloseBtn;

    @FindBy(xpath = "//span[contains(text(), 'The field is required.')]")
    public WebElement requiredFieldWarning;

    @FindBy(xpath = "//article[contains(@class, 'card-jobsHot')]//h2")
    public List<WebElement> positionsAvailable;

    @FindBy(xpath = "//article[contains(@class, 'card-jobsHot')]//a")
    public List<WebElement> moreInfo;


    //Init Elements
    public CareersPage(){
        PageFactory.initElements(driver, this);
    }

    //METHODS

    public void uploadFile(){
        String filePath = System.getProperty("user.dir") + "\\src\\main\\java\\com\\qa\\util\\uploadFile.docx";
        File f = new File(filePath);

        el.setText(careersUploadCv, filePath);
        Assert.assertEquals(f.getName(), "uploadFile.docx");
    }

    public void locationSelector(String location){
        el.listElements(locations, location);
    }

    public void availablePositions(String text) {
        String position = "";
        String moreInfo = "";
        System.out.println(":::::" + text + ":::::");
        for (WebElement elem : this.moreInfo) {
            moreInfo = elem.getAttribute("href");
            position = elem.getText().split("\n")[0];
            System.out.println("Position: " + position);
            System.out.println("More Info: " + moreInfo + "\n");
        }

    }
}
