package com.qa.pages;

import com.qa.base.TestBase;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class HomePage extends TestBase {

    //ELEMENTS
    @FindBy(xpath = "//span[text() = 'Contact us']")
    public WebElement contactUs;

    @FindBy(id = "cf-1")
    public WebElement contactUsName;

    @FindBy(id = "cf-2")
    public WebElement contactUsEmail;

    @FindBy(id = "cf-4")
    public WebElement contactUsSubject;

    @FindBy(id = "cf-5")
    public WebElement contactUsMessage;

    @FindBy(xpath = "//input[@type = 'submit']")
    public WebElement sendBtn;

    @FindBy(xpath = "//span[contains(text(), 'The e-mail')]")
    public WebElement emailValidation;

    //Header Elements
    @FindBy(xpath = "//div[@id = 'menu']//a[text() = 'Company']")
    public WebElement company;

    @FindBy(xpath = "//div[@id = 'menu']//a[text() = 'Careers']")
    public WebElement careers;

    //Init Elements
    public HomePage(){
        PageFactory.initElements(driver, this);
    }

    //Methods
}
