package com.qa.tests;

import com.qa.base.TestBase;
import com.qa.pages.CareersPage;
import com.qa.pages.CompanyPage;
import com.qa.pages.HomePage;
import com.qa.util.ElementUtil;
import com.qa.util.TestUtil;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CareersPageTest extends TestBase {

    CompanyPage cp;
    HomePage hp;
    ElementUtil el;
    CareersPage crp;

    public CareersPageTest(){
        super();
    }

    @BeforeMethod
    public void setUp(){
        initialization();
        cp = new CompanyPage();
        hp = new HomePage();
        el = new ElementUtil();
        crp = new CareersPage();
    }

    @Test(testName = "Invalid email on user application")
    public void invalidEmailUserApplication() throws InterruptedException {
        el.clickElement(hp.careers);
        el.clickElement(crp.openPositions);
        Assert.assertEquals(driver.getCurrentUrl(), TestUtil.CAREERS_PAGE);

        crp.locationSelector("Anywhere");
        el.clickElement(crp.qaEngineer);

        Assert.assertEquals(el.getElementText(crp.generalDescription), "General description");
        Assert.assertEquals(el.getElementText(crp.requirements), "Requirements");
        Assert.assertEquals(el.getElementText(crp.responsibilities), "Responsibilities");
        Assert.assertEquals(el.getElementText(crp.whatWeOffer), "What we offer");

        Assert.assertEquals(crp.applyBtn.getAttribute("value"), "Apply");
        el.clickElement(crp.applyBtn);

        el.setText(crp.careersName, "Automation");
        el.setText(crp.careersEmail, "automation@test");
        el.setText(crp.careersMobile, "555-555-5555");
        crp.uploadFile();

        el.clickElement(crp.careersSendBtn);
        el.clickElement(crp.careersCloseBtn);

        Assert.assertEquals(el.getElementText(hp.emailValidation), TestUtil.REQUIRED_EMAIL_WARNING);
        Assert.assertEquals(el.getElementText(crp.requiredFieldWarning), TestUtil.REQUIRED_FIELD_WARNING);
    }

    @Test(testName = "Available positions by city")
    public void availablePositionsByCity(){
        el.clickElement(hp.careers);
        el.clickElement(crp.openPositions);

        crp.locationSelector("Sofia");
        crp.availablePositions("Sofia");

        crp.locationSelector("Skopje");
        crp.availablePositions("Skopje");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
