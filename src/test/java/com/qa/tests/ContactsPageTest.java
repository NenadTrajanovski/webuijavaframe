package com.qa.tests;

import com.qa.base.TestBase;
import com.qa.pages.HomePage;
import com.qa.pages.CompanyPage;
import com.qa.util.TestUtil;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.util.ElementUtil;

public class ContactsPageTest extends TestBase {

    CompanyPage cp;
    HomePage hp;
    ElementUtil el;

    public ContactsPageTest(){
        super();
    }

    @BeforeMethod
    public void setUp(){
        initialization();
        cp = new CompanyPage();
        hp = new HomePage();
        el = new ElementUtil();
    }

    @Test(testName = "Facebook profile url and image verification")
    public void verifyFacebookProfileLoad(){
        el.clickElement(hp.company);
        Assert.assertEquals(driver.getCurrentUrl(), TestUtil.COMPANY );
        Assert.assertEquals(el.getElementText(cp.leadershipHeader), "Leadership");

        el.clickElement(cp.facebook);
        el.switchBrowserTabs(1);
        Assert.assertEquals(driver.getCurrentUrl(), TestUtil.FACEBOOK);
        Assert.assertEquals(cp.facebookProfilePicture.getAttribute("src"), TestUtil.FACEBOOK_PROFILE_PICTURE);

    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
