package com.qa.tests;

import com.qa.base.TestBase;
import com.qa.pages.HomePage;
import com.qa.util.ElementUtil;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageTest extends TestBase {

    HomePage hp;
    ElementUtil el;

    public HomePageTest(){
        super();
    }

    @BeforeMethod
    public void setUp(){
        initialization();
        hp = new HomePage();
        el = new ElementUtil();
    }

    @Test(dataProvider = "readUserLoginData", testName = "Invalid email address validation")
    public void invalidEmailAddress(String email){
        el.scrollAndClickElement(hp.contactUs);
        el.setText(hp.contactUsName, "Automation User");
        el.setText(hp.contactUsEmail, email);
        el.setText(hp.contactUsSubject, "This is my application");
        el.setText(hp.contactUsMessage, "This is my automation test");
        el.clickElement(hp.sendBtn);

        Assert.assertEquals(el.getElementText(hp.emailValidation), "The e-mail address entered is invalid.");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
