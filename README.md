# WebUIJavaFrame

Web UI automation frame using Java, TestNG & Selenium WebDriver

### Please read this document carefully before running the frame on your local machine

###### Driver information:
- Frame is configured to run with ChromeDriver 89 and Chrome Version 89
    - Please upgrade or downgrade accordingly
        - https://chromedriver.chromium.org/
        - https://github.com/mozilla/geckodriver/releases
    
- Both chrome and gecko drivers are set in the root directory under webdrivers folder
    - In case drivers are not set via environment variables they can be accessed via:
    - `System.getProperty("user.dir") + "\\webdrivers\\geckodriver.exe"`

###### Frame configuration

Every test is driven from the `TestBase.java` class located under `main.java.com.qa.base` package and configurable
from `config.properties` file stored under `main.java.com.qa.config`

###### Test Execution

Please use xml files to execute test, or do on a test/class basis within the test classes
located under `test.java.com.qa.tests.*` classes